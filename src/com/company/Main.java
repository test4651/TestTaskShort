package com.company;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {

        Client client1 = new Client("Boris Britva", 100.00, 0);
        Client client2 = new Client("Vova Krab", 30.00, 0);
        Client client3 = new Client("Boris Britva", 100.00, 0);
        Client client4 = new Client("Vlad Korpan", 100.00, 0);
        Client client5 = new Client("Anton Beton", 100.00, 0);
        Client client6 = new Client("whatever whatever", 100.00, 0);
        Client client7 = new Client("kek lol", 100.00, 0);

        ArrayList<Event> events = new ArrayList<>();
        events.add(new Event(
                "Футбол Собаки-Коти",
                LocalDate.of(2017, Month.APRIL, 14),
                15.00f,
                12));
        events.add(new Event(
                "Dota 2 international",
                LocalDate.of(2017, Month.JANUARY, 12),
                15.00f,
                12));
        events.add(new Event(
                "CSGO international",
                LocalDate.of(2016, Month.JANUARY, 1),
                15.00f,
                12));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        events.sort(Comparator.comparing(Event::getDate));
        for (int i = 0; i < events.size(); i++) {
            System.out.println((i + 1) + ". " + events.get(i).getName() + " " + events.get(i).getDate().format(formatter));
        }

        events.get(0).sellEventTicket(client1,4);
        events.get(1).sellEventTicket(client3,3);
        events.get(0).sellEventTicket(client5,6);
        events.get(2).sellEventTicket(client7,6);


        for(int i = 0; i < events.size();i++){
            System.out.println(events.get(i).getName()+":");
            for(int j  = 0; j < events.get(i).getAttendees().size();j++){
                System.out.print( events.get(i).getAttendees().get(j).getName()+" ");
                System.out.print( events.get(i).getAttendees().get(j).getTicketCount()+" шт. \n");
            }
        }
    }
}
