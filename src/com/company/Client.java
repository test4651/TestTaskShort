package com.company;

public class Client {

    private String name;
    private double cash;
    private int ticketCount;

    public Client( String name,double cash, int ticketCount) {
        this.name = name;
        this.cash = cash;
        this.ticketCount = ticketCount;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setCash(double cash){
        this.cash = cash;
    }

    public double getCash(){
        return this.cash;
    }

    public void setTicketCount(int ticketCount){
        this.ticketCount = ticketCount;
    }

    public int getTicketCount() {
        return this.ticketCount;
    }
}
