package com.company;


import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;

public class Event {

    private String name;
    private LocalDate date;
    private float ticketCost;
    private int ticketStock;
    private ArrayList<Client> attendees;

    public Event(){};

    public Event(String name, LocalDate date, float ticketCost, int ticketStock){
        this.name = name;
        this.date = date;
        this.ticketCost = ticketCost;
        this.ticketStock = ticketStock;
        this.attendees = new ArrayList<>();
    }

    public void setName(String eventName){
        this.name = eventName;
    }

    public String getName(){
        return this.name;
    }

    public void setDate(LocalDate eventDate){
        this.date = eventDate;
    }

    public LocalDate getDate(){
        return this.date;
    }

    public void setTicketCost(float ticketCost){
        this.ticketCost = ticketCost;
    }

    public float getTicketCost(){
        return this.ticketCost;
    }

    public void setTicketStock(int ticketStock){
        this.ticketStock = ticketStock;
    }

    public int getTicketStock(){
        return this.ticketStock;
    }

    public void setAttendees(ArrayList<Client> attendees){
        this.attendees = attendees;
    }

    public ArrayList<Client> getAttendees(){
        return this.attendees;
    }

    public void sellEventTicket(Client client, int numberOfTickets){
        try{
            if (client.getCash() <= 0) throw new Exception("you're broke!");}
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        try{
            if(this.ticketStock<= 0) throw  new Exception("Out of stock!");}
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        try{
            if(numberOfTickets > this.ticketStock) throw  new Exception("Dont have enough tickets for transaction!");}
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        try{
            if (client.getCash() >= (this.ticketCost*numberOfTickets)){
                client.setCash(client.getCash() - (this.ticketCost * numberOfTickets));
               this.ticketStock = this.ticketStock - numberOfTickets;
                client.setTicketCount(numberOfTickets);
                this.attendees.add(client);
            }
            else throw new Exception("not enough cash!");
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }


}
